# ссылка на бота в телеграм
# https://t.me/itiscloudpr_bot

import json
import os
import requests


def handler(event, context):
    def weather(latitude_, longitude_, weather_api_key_):
        wheather_url = "https://api.openweathermap.org/data/2.5/weather"
        wheather_response = requests.get(wheather_url, params = {
            'lat': latitude_,
            'lon': longitude_,
            'appid': weather_api_key_,
            'units':'metric',
            'lang':'ru'})

        temp_C = round(float(wheather_response.json()["main"]["temp"]), 1)
        feels_like_temp_C = round(float(wheather_response.json()["main"]["feels_like"]), 1)
        description_weather = wheather_response.json()["weather"][0]["description"]
        humidity_weather = wheather_response.json()["main"]["humidity"]
        pressure_weather = wheather_response.json()["main"]["pressure"] * 0.75
        wind_speed = wheather_response.json()["wind"]["speed"]
        wind_deg = wheather_response.json()["wind"]["deg"]
        wind_gust = wheather_response.json()["wind"]["gust"]
        cloudiness = wheather_response.json()["clouds"]["all"]

        chat_id = update["message"]["chat"]["id"]
        message_id = update["message"]["message_id"]

        text = f"Погода: {description_weather}\n"\
            f"Температура: {temp_C} \u2103\nПо ощущению: {feels_like_temp_C} \u2103\n"\
            f"Атмосферное давление: {pressure_weather} мм рт.ст.\n" \
            f"Влажность: {humidity_weather} %\nСкорость ветра: {wind_speed} м/с\n" \
            f"Направление ветра: {wind_deg} градусов\nПорывы ветра: {wind_gust} м/с\n" \
            f"Облачность: {cloudiness} %"

        reply_to_message = requests.post(
            f"{url_tlgrm}sendMessage",
            json={
                'chat_id': chat_id, 
                'text': text, 
                'reply_to_message_id':message_id
                })

        return reply_to_message

    def wrong_in_():
        chat_id = update["message"]["chat"]["id"]
        message_id = update["message"]["message_id"]
        reply_to_message = requests.post(
            f"{url_tlgrm}sendMessage",
            json={
                'chat_id': chat_id, 
                'text': "неправильный запрос", 
                'reply_to_message_id':message_id
                })
        return reply_to_message

    update = json.loads(event["body"])
    
    dadata_api_key = os.environ["DADATA_API_KEY"]
    dadata_secret_key = os.environ["DADATA_SECRET_KEY"]
    weather_api_key = os.environ["WEATHER_API_KEY"]
    telegram_token = os.environ["TELEGRAM_TOKEN"]

    url_tlgrm = "https://api.telegram.org/bot" + telegram_token + "/"

    if "text" in update["message"]:
        if update["message"]["text"] != "/start":
            try:
                address = update["message"]["text"]
                dadata_response = requests.post(
                    'https://cleaner.dadata.ru/api/v1/clean/address',
                    json=[address], 
                    headers={
                        'Authorization': "Token " + dadata_api_key, 
                        'X-Secret': dadata_secret_key
                        })
                latitude = dadata_response.json()[0]["geo_lat"]
                longitude = dadata_response.json()[0]["geo_lon"]
                weather(latitude, longitude, weather_api_key)

            except:
                wrong_in_()
        
    elif "location" in update["message"]:
        longitude = update["message"]["location"]["longitude"]
        latitude = update["message"]["location"]["latitude"]
        weather(latitude, longitude, weather_api_key)

    else:
        wrong_in_()

    return {
        'statusCode': 200,
    }
